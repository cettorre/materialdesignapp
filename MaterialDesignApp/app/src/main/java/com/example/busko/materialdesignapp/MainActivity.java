package com.example.busko.materialdesignapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG ="AppCompatActivity";

    private static final String EXTRA_UPDATE ="update";
    private static final String EXTRA_NAME ="name";
    private static final String EXTRA_DELETE ="delete";
    private static final String EXTRA_COLOR ="color";
    private static final String EXTRA_INITIAL ="initial";

    private static final String TRANSITION_FAB ="fab_transition";
    private static final String TRANSITION_INITIAL ="initial_transition";
    private static final String TRANSITION_NAME ="name_transition";
    private static final String TRANSITION_DELETE_BUTTON ="delete_button_transition";

    private RecyclerView recyclerView;
 //   private SampleMateialAdapter adapter;
//    private ArrayList<Card> cardList = new ArrayList<>();
    private int [] colors;
    private String[] names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       names= getResources().getStringArray(R.array.names_array);
       colors = getResources().getIntArray(R.array.initial_colors);

     //  initCards(); //v5@2:26

  //     if(adapter == null){
 //          adapter = new SampleMaterialAdapter(this,cardList);
//       }

    }

    private void initCards(){
        for(int i=0;i<50;i++){
            Card card = new Card();
            card.setId((long)i);
            card.setName(names[i]);

        }
    }


}
